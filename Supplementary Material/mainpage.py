## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage Home
#
#  @section sec_port Portfolio Details  
#  This is a secondary ME405 portfolio. Due to unfortunate circumstances and timing,
#  files necessary for generating and  maintaining the previous portfolio have been loss.
#  This portfolio focuses on documenting the term project for ME405. To access the 
#  rest of the portfolio dedicated to ME405, please refer to https://aartran23.bitbucket.io/
#
#  For easy access from this portfolio, the following hyperlinks have been provided:
#  * ME405 Overview https://aartran23.bitbucket.io/index.html#intro_4
#  * Lab0x01        https://aartran23.bitbucket.io/index.html#nLab1
#  * Lab0x02        https://aartran23.bitbucket.io/index.html#nLab2
#  * Lab0x03        https://aartran23.bitbucket.io/index.html#nLab3
#  * HW0x02         https://aartran23.bitbucket.io/page_HW2.html
#  * HW0x04         https://aartran23.bitbucket.io/page_HW4.html
#  * HW0x05         https://aartran23.bitbucket.io/page_HW5.html
#  * \ref term_proj "Term project"
#
## \page term_proj "Term Project "
#
#  The rest of this page will be dedicated to 
#  @section intro Lab0x00 Documentation
#  * Source: https://bitbucket.org/erwade/examplefiles/src/master/Lab0.py
#  * Documentation: \ref Lab0
#
#  @section sec_lab1 Lab0x01 Documentation 
#  * Source: https://bitbucket.org/erwade/examplefiles/src/master/Lab1.py
#  * Documentation: \ref Lab1
#
#  @section sec_labn Lab0x0N Documentation
#  * Source: https://bitbucket.org/erwade/examplefiles/src/master/LabN.py
#  * Documentation: \ref LabN
#
#  @author Aaron Tran
#
#  @date June 6, 2021
#