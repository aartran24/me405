## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage Home
#
#  @section sec_port Portfolio Details  
#  This is a secondary ME405 portfolio. Due to unfortunate circumstances and timing,
#  files necessary for generating and  maintaining the previous portfolio have been loss.
#  This portfolio focuses on documenting the term project for ME405. To access the 
#  rest of the portfolio dedicated to ME405, please refer to https://aartran23.bitbucket.io/
#
#  For easy access from this portfolio, the following hyperlinks have been provided:
#  * ME405 Overview https://aartran23.bitbucket.io/index.html#intro_4
#  * Lab0x01        https://aartran23.bitbucket.io/index.html#nLab1
#  * Lab0x02        https://aartran23.bitbucket.io/index.html#nLab2
#  * Lab0x03        https://aartran23.bitbucket.io/index.html#nLab3
#  * HW0x02         https://aartran23.bitbucket.io/page_HW2.html
#  * HW0x04         https://aartran23.bitbucket.io/page_HW4.html
#  * HW0x05         https://aartran23.bitbucket.io/page_HW5.html
#  * \ref term_proj "Term project"
#
## \page term_proj Term Project
#
#  @section intro Introduction
#  The purpose of the term project is to use mechatronic concepts, alongside control systems theory, to write the code necessary to
#  balance a ball on a platform using two motors. To do so, the homework assignments provided in the \ref sec_port "home page" were completed,
#  with the intention of designing the control system. The ultimate outcome is a fullstate feedback controller that takes the sensor readings for the 
#  position and velocity of the ball, as well as the angle and angular velocity of the table and sends PWM signals to the motors accordingly. A screenshot
#  of the CAD of the table to be used is provided in Figure 1 below:
#  @image html Table.png "Figure 1. ME305/405 Balancing platform"
#   
#  @section code Code
#  The code responsible for this project consists of three drivers, one scheduler task, and one script that 
#  contains a set of cooperative tasks that utilize the three driver. For a brief overview of main.py, the script that contains all task definitions,
#  and puts them together, refer to the task diagram shown in Figure 2 below:
#  @image html TaskDia.png "Figure 2. main.py Task Diagram"
#   
#  To access the code documentation, refer to EncoderDriver.py, MotorDriver.py, TouchDriver.py, cotask.py, and main.py. The documentation
#  also includes access to the source code, if such is desired.
#
#  @section operation Operation Guide
#  First, level the platform as best as possible. Once the platform has been leveled, the code can be ran. Press q and e to calibrate the motor position an encoder, respectively.
#  Once the terminal has indicated that motor 1 has been calibrated, level the platform again and hit enter. Once the second motor has been calibrated, feel free to 
#  do whatever you like with it. Various user interface commands have been built into the code, as listed below:
#  * c      - clearing faults
#  * i      - enable motors
#  * o      - disable motors
#  * w      - increase position tuning constant by 0.05
#  * a      - decrease position tuning constant by 0.05
#  * s      - increase speed tuning constant by 0.05
#  * d      - decrease speed tuning constant by 0.05
#  * z      - begin collecting data/ stop collecting and clear collected data
#  * x      - display current PWM being sent to motors
#  * v      - display current state vector values 
#  * e      - zero encoders
#  * q      - zero ball position
#   
#  Note that running data collection for too long will result in all of the allocated space being taken up. The DAQ has been designed to be able to collect
#  data for 20s. To retrieve collected data the user must press Ctrl+C.
#  @section demo Demonstration
#  A demonstration of the term project, as well as some discussion is provided in the video below:\n
#  https://cpslo-my.sharepoint.com/:v:/g/personal/atran189_calpoly_edu/Ea5I7CJP6yNNhD9ikSxSIPQBqXJhcum5Is6mlJYMC0U3ow?e=0U5UHk
#
#  The data retrieved in the video can be seen in the plot in Figre 3 below:
#  @image html data.png "Figure 3. Data collected during demonstration"
#  
#  The image shown above corresponds to the data collected during the demonstration, though the plot does not match what was plotted in the video. For some reason
#  the data did not get plotted correctly when selecting the entire column of data, which I noticed by looking at the scale of the x axis. It reaches points that
#  the data does not have. I do not know what happened, but this is an accurate representation of the platform's efforts. After rewatching the video, I can confirm that 
#  the data matches the actions of the ball well.
#  
#  @section tune Controller Tuning
#  All initial gain values were determined analytically. These were determined in the homework assignments listed in the \ref sec_port "Portfolio Details".
#
#  In terms of motor tuning, I found that having higher gains for the table angular speed would create greater oscillations and made it more difficult for the table to balance by itself. Seeing
#  as the table needs to at least be able to level itself before it can handle balancing the ball, I determined that this specific gain should be minimized. Relative to the theoretical gains determined
#  by modeling the system, this gain is the only one that was reduced. I suspect that the reason for oscillation is that the controller applies too high of a PWM signal which in turn creates a high table velocity. 
#  This then creates another high PWM signal in the opposite direction. This cycles back and forth creating an oscillation. Additionally, I had tuning parameters setup for adjusting both of the position gains and both of the
#  speed gains. This let me freely control how well the table responds to disturbances using the position gains and how well the response is damped using the speed gains. Additionally, as discussed in the video, I tried
#  to leave larger gains for the angular position, as I believe it is vital that the platform can balance by itself. I also think that this helps prevent the table from going past the small angle approximation as it strongly
#  prevents large angular displacements. For adjusting the x and y linear position gains, I tried to just increase them if I found that the ball was moving far out without the table responding strongly. I would lower it if it
#  was causing a lot of motor fault detections. Finally for the linear velocity gains, I would increase them if it looked like the ball was just swinging from one side to the other really quickly. This helped damp the response and slow the ball
#  down as it approached the center of the platform. 
#
#  @section disc Discussion
#  I was unfortunately unable to demonstrate a fully functioning table, but I think that the results were close. However, there were a lot of difficulties with this project that I would like to discuss. 
#  One of special interest is the dead zone we see in the motor. With low PWM signals, we do not
#  see any movement of the motor shaft. I believe that this is because of the inherent stiction inside of the motor. To offset this, main.Stiction() was written.
#  The way this function works is that the motor will gradually apply greater and greater PWM until the encoder reads
#  a small change in the encoder angle. This PWM is then added to the magnitude of all PWM signals. Given that the stiction in the beginning is a constant resistance
#  that the motor sees, this would be a valid approach, but I believe that the resistance the motor sees is not constant. I found that when hand twisting the motor shaft,
#  the shaft would be difficult to twist about half of the time. This is likely due to the pulley not being the best. It becomes warped overtime and offers varying resistance based off of 
#  how it is positioned on the shaft. This caused a great amount of difficulty with the motor control, especially, when attempting to come close to equilbrium. The PWM would be too weak
#  when the ball was near the center of the platform which would leave the table in a tilted state. This then allowed the ball to continue to roll. By the time the table could react to
#  this, the ball would fly off. The table even finds difficulty coming to a flat state without a ball on it. When its close, the PWM is just too weak to make the platform move. Occasionall,
#  it can correct, but it would over correct. At other times, the table would just oscillate instead, as it would continuously overcorrect.
#
#  Another challenge that arises from the oscillation when coming to an almost level platform, is that I believe it is somehow interfering with the encoder reading. The zero position of the angles appears to drift
#  under normal testing the EncoderDriver.py works great. It measures accurately without a problem. However, while testing to make sure that the table could balance by itself, I found that 
#  as I tested it, the point it would try to oscillate about would slowly become more and more tilted. The more time it spent oscillating, the more it would drift. I suspect that the pulley is slipping, so while the encoder is
#  in its zero position the table's zero position drifts.
#
#
#  I would like to note that while the version of the code that I demonstrated was unable to balance the ball from rest in the middle, that this is only because I chose to provide a slightly more agressive
#  solution for dealing with the motor's dead zone. I add a constant calibration constant to the magnitudes of the motors' PWM signals to get past the deadzone. I have tried using slightly agressive methods
#  such as using a piecewise function with three sections. Two sections describe the agressive method. These sections are on the sides of a PWM request to PWM output plot. In the center is a section that passes through the origin,
#  but has a large slope, such that at a designated PWM request, it reaches the value of the agressive method. An example of this is shown in Figure 4 below:
#
#  @image html examplePWM.png "Figure 4. Attempt of dead zone compensation method"
#
#  However, due to the high amount of noise in the touch panel data, the selected value had to be selected at a PWM of 5.5. Any lower than this
#  and the PWM would jump to quickly into the agressive zone. This did allow for the ball to balance from rest, but had much more difficulty with trying to balance the ball from other positions on the platform. For this reason,
#  I went ahead and used the more agressive, constant PWM offset from the controller calculation. For some unknown reason, this was easier to tune for.
#
#  
#  @author Aaron Tran
#
#  @date June 6, 2021
#