'''@file        EncoderDriver.py
@brief          Reads encoder sensor data
@details        Source code can be found at https://bitbucket.org/aartran24/me405/src/master/405%20Term%20Project/EncoderDriver.py
@author         Aaron Tran
@date           6/3/21
'''

import utime
import pyb
import math
class EncoderDriver:
    '''Implements a motor encoder class for the ME405 board.
    '''
    def __init__(self, pin1, pin2, timer):
        '''@param pin1 one of two pins that physical encoder is connected to
        @param pin2 one of two pins that physical encoder is connected to
        @param timer timer corresponding to two pins used for physical encoder
        '''
        ##@brief one of two pins that physical encoder is connected to
        self.pin1 = pin1
        ##@brief one of two pins that physical encoder is connected to     
        self.pin2 = pin2
        ##@brief timer corresponding to two pins used for physical encoder
        self.timer = timer
        
        ##@brief timer channel to enable counting in one direction
        self.ch1 = self.timer.channel(1,self.timer.ENC_AB, pin=self.pin1)
        ##@brief timer channel to enable counting in another direction
        self.ch2 = self.timer.channel(2,self.timer.ENC_AB, pin=self.pin2)
        ##@brief current time of system
        self.thisTime = utime.ticks_ms()
        ##@brief current count of encoder
        self.ctNow = self.timer.counter()
        ##@brief position of encoder
        self.theta = 0
        ##@brief most recent change in encoder position
        self.delta = 0

    def run(self):
        '''@brief Runs to constantly update motor position on 20ms interval
        '''
        ##@brief global time of system
        self.globT = utime.ticks_ms()
        if utime.ticks_diff(self.globT,self.thisTime) >= 20:
            self.update()
            self.thisTime = utime.ticks_ms()
                
    def update(self):
        '''@brief updates recorded position of encoder
        '''
        ##@brief uncorrected delta value
        self.delta_check = self.timer.counter() - self.ctNow    
        # self.delta = self.tim4.counter() - self.ctNow    
        if self.delta_check > -0xFFFF/2 and self.delta_check < 0xFFFF/2:
            pass
        elif self.delta_check >= 0xFFFF/2:
            self.delta_check -= 0xFFFF   
        elif self.delta_check <= -0xFFFF/2:
            self.delta_check += 0xFFFF       
        pass    
        self.delta = self.delta_check*2*math.pi/4000    
        self.theta += self.delta
        self.ctNow = self.timer.counter()

    def get_position(self):
        '''@brief returns most recently updated position of encoder
        '''
        return(self.theta)
    
    def set_position(self,pos):
        '''@brief resets position to specified value
        @param pos desired position to set encoder value to
        '''
        self.theta = pos
    
    def get_delta(self):
        '''@brief returns difference in recorded position between two most recent calls to update
        '''
        return(self.delta)
    
    
if __name__ == "__main__":
    #This portion is intended to be used for testing purposes only
    ##@brief Encoder 1 pin 1
    ENC1_1 = pyb.Pin.cpu.B6
    ##@brief Encoder 1 pin 2    
    ENC1_2 = pyb.Pin.cpu.B7
    ##@brief Encoder 1 pin 1    
    ENC2_1 = pyb.Pin.cpu.C6
    ##@brief Encoder 1 pin 2    
    ENC2_2 = pyb.Pin.cpu.C7
    ##@brief Timer 4 object, for use on encoder 1    
    tim4 = pyb.Timer(4, period=0xFFFF, prescaler=0)
    ##@brief Timer 8 object, for use on encoder 2
    tim8 = pyb.Timer(8, period=0xFFFF, prescaler=0)
    
    ##@brief EncoderDriver instance for encoder 1
    enc1 = EncoderDriver(ENC1_1, ENC1_2, tim4)
    ##@brief EncodeRDriver instance for encoder 2
    enc2 = EncoderDriver(ENC2_1, ENC2_2, tim8)
    
    ##@brief timestamp for periodically running code
    now = utime.ticks_ms()
    #Test code will print encoder angles once every second
    while True:
        if utime.ticks_diff(utime.ticks_ms(), now) >=1000:    
            enc1.update()
            enc2.update()
            print('Encoder 1: ' + str(enc1.get_position()))
            print('Encoder 2: ' + str(enc2.get_position()))            
            now = utime.ticks_ms()