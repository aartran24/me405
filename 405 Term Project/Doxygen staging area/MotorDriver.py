'''@file        MotorDriver.py
@brief          Controls motor PWM
@details        Source code can be found at https://bitbucket.org/aartran24/me405/src/master/405%20Term%20Project/MotorDriver.py
@author         Aaron Tran
@date           5/20/21
'''

import pyb
import utime
class DRV8847:
    '''Implements a motor driver class for the ME405 balancing platform board
    '''
    def __init__ (self, nSLEEP_pin, nFAULT_pin):
        '''@param nSLEEP_pin micropython object representing the nSLEEP pin on the DRV8847 motor
        @param nFAULT_pin micropython object representing the nFAULT pin on the DRV8847 motor
        '''
        ##@brief nSLEEP pin
        self.nSLEEP = pyb.Pin(nSLEEP_pin)
        ##@brief nFaULT_pin configured for interrupt service routine on a falling edge
        self.Fault = pyb.ExtInt(nFAULT_pin, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback= self.fault_CB)
        ##@brief state of motor (disabled due to fault or not)
        self.fault = False
    
    def enable (self):
        '''sets nSLEEP_pin to high to enable motors
        '''
        ##@brief Flag for motor initial motor startup
        self.initial = True
        if self.fault:
            print('Fault detected, unable to enable motors')
        else:
            self.nSLEEP.init(mode=pyb.Pin.OUT_PP, value=1)
    
    def disable (self):
        '''sets nSLEEP_pin to low to disable motors
        '''
        self.nSLEEP.init(mode=pyb.Pin.OUT_PP, value=0)
    
    def fault_CB (self, IRQ_src):
        '''Callback function for interrupt service routine, triggered by nFAULT pin falling edge. Disables motors until fault is cleared.
        '''
        if self.initial:
            self.initial = False
        else:
            self.disable()
            self.fault = True
            print('Fault detected')
        
    def clear_fault (self):
        '''Clears fault condition, so that motors may be reenabled
        '''
        if self.fault:
            self.fault = False
            print('Fault cleared')
        else:
            print('No fault detected')
        
    def channel (self, INx_pin, INy_pin, INxy_timer):
        '''Instantiates DRV8847_channel class within the DRV8847 class
        '''
        return DRV8847_channel(INx_pin, INy_pin, INxy_timer)
    
class DRV8847_channel:
    '''Sets up timer channels as needed for each motor to pass in to the DRV8847 class
    '''
    def __init__ (self, INx_pin, INy_pin, INxy_timer):
        '''@param INx_pin pin corresponding to one of the motor's voltage terminals
        @param INy_pin pin corresponding to the other voltage terminal of the motor
        @param INxy_timer timer to be used for motor PWM
        '''
        ##@brief timer to be used for motor PWM
        self.timer = INxy_timer
        #Selects motor channels based off of what set of pins are passed in
        if INx_pin == pyb.Pin.cpu.B4 or INx_pin == pyb.Pin.cpu.B5:
            __INxCH__ = 1
            __INyCH__ = 2
        elif INx_pin == pyb.Pin.cpu.B0 or INx_pin == pyb.Pin.cpu.B1:
            __INxCH__ = 3
            __INyCH__ = 4
        else:
            print('Incompatible pins provided')
        ##@brief timer channel used for one direction of motion
        self.INx = self.timer.channel(__INxCH__, mode=pyb.Timer.PWM, pin=INx_pin)
        ##@brief timer channel used for opposite direction of motion from self.IN1       
        self.INy = self.timer.channel(__INyCH__, mode=pyb.Timer.PWM, pin=INy_pin)        
        
    
    def set_level (self, level):
        '''@brief sets PWM of motor 
        @details This method sets the duty cycle to be sent to the motor to the given level. Positive values
        cause effort in one direction, negative values in the opposite direction.
        @param level A signed integer holding the duty
        cycle of the PWM signal sent to the motor '''
        if level >= 0:
            ##@brief PWM to be input into the INx_pin
            PWM_x = level
            ##@brief PWM to be input into the INy_pin
            PWM_y = 0
        elif level < 0:
            PWM_x = 0
            PWM_y = abs(level)
        self.INx.pulse_width_percent(PWM_x)
        self.INy.pulse_width_percent(PWM_y)
    
if __name__ == "__main__":
    #This code is intended to be used for testing purposes only
    ##@brief UART port
    myuart = pyb.UART(2)    
    #Hardware configuration
    ##@brief Motor 1 INx pin
    IN1 = pyb.Pin.cpu.B4
    ##@brief Motor 1 INy pin    
    IN2 = pyb.Pin.cpu.B5
    ##@brief Motor 2 INx pin    
    IN3 = pyb.Pin.cpu.B0
    ##@brief Motor 2 INy pin    
    IN4 = pyb.Pin.cpu.B1    
    ##@brief nSLEEP pin for both motors
    nSLEEP = pyb.Pin.cpu.A15
    ##@brief nFAuLT pin for both motors
    nFAULT = pyb.Pin.cpu.B2
    ##@brief Timer 3, to be used for PWM
    tim = pyb.Timer(3, freq=20000)
    
    #Instantiating classes
    ##@brief DRV8847 instance
    mot = DRV8847(nSLEEP, nFAULT)
    ##@brief DRV8847_channel instance for motor 1
    mot1 = mot.channel(IN1,IN2,tim)
    ##@brief DRV8847_channel instance for motor 2   
    mot2 = mot.channel(IN3,IN4,tim)
    
    mot.enable()
    ##@brief ASCII value sent through UART
    val = None
    ##@brief motor PWM
    duty = 50
    
    #Powering motors
    ##@brief timestamp for periodically running code
    now = utime.ticks_ms()
    while True:
        #This is to check how much PWM is necessary to overcome stiction
        if utime.ticks_diff(utime.ticks_ms(), now)>=500:    
            duty += 1
            mot1.set_level(duty)
            mot2.set_level(duty)
            print(duty)
            now = utime.ticks_ms()
        #this potion is for user interface
        if myuart.any() != 0:
            val = myuart.readchar()
            print('You pressed ASCII' + str(val))
        if val == 103:
            # press g to clear fault
            mot.clear_fault()
            val = None
            print('Clearing Fault')
        elif val == 114:
            # press r to enable
            mot.enable()
            val = None
            print('Enabling Motor')
        elif val == 100:
            # press d to disable
            val = None
            mot.disable()
            print('Disabling Motor')
            
    
    
    