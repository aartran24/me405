'''@file        TouchDriver.py
@brief          Reads resistive touch panel
@details        Source code can be found at https://bitbucket.org/aartran24/me405/src/master/405%20Term%20Project/TouchDriver.py
@author         Aaron Tran
@date           5/20/21
'''
import pyb
import utime

class TouchDriver:
    '''
    Reads position of objects in x and y on resistive touch panel
    '''
    def __init__(self,P1, P2, P3, P4, length, width, cc):
        '''
        Initializes TouchDriver class object
        @param P1 CPU connected to ym
        @param P2 CPU connected to xm
        @param P3 CPU connected to yp
        @param P4 CPU connected to xp
        @param length length (x-dir) of touch panel
        @param width width (y-dir) of touch panel
        @param cc list representing [xc,yc], the position of the origin relative to the bottom left corner of the board
        '''
        ##@brief CPU pin connected to ym
        self.ym = pyb.Pin(P1, mode = pyb.Pin.OUT_PP, value = 0)
        ##@brief CPU pin connected to xm
        self.xm = pyb.Pin(P2, mode = pyb.Pin.OUT_PP, value = 0)
        ##@brief CPU pin connected to yp
        self.yp = pyb.Pin(P3, mode = pyb.Pin.OUT_PP, value = 0)
        ##@brief CPU pin connected to xp
        self.xp = pyb.Pin(P4, mode = pyb.Pin.OUT_PP, value = 0)
        ##@brief configure xm fpr ADC
        self.xADC = pyb.ADC(self.ym)
        ##@brief configure ym for ADC
        self.yADC = pyb.ADC(self.xm)
        ##@brief length of touch panel
        self.length = length
        ##@brief width of touch panel
        self.width = width
        ##@brief calibrated x origin position relative to bottom left corner (manually preset)
        self.xc = cc[0]
        ##@brief calibrated y origin position relative to bottom left corner (manually preset)
        self.yc = cc[1]
        
        ##@brief additional x callibrations (automatically calculated using TouchDriver.cal())
        self.xcal = 0
        ##@brief additional ycallibrations (automatically calculated using TouchDriver.cal())
        self.ycal = 0
        
    
    def x_scan(self):
        '''
        Reads x position
        '''
        self.xm.init(mode = pyb.Pin.OUT_PP, value = 0)
        self.xp.init(mode = pyb.Pin.OUT_PP, value = 1)
        self.ym.init(mode=pyb.Pin.ANALOG)
        self.yp.init(mode=pyb.Pin.IN)
        
        self.pause(5)
        self.xADC = pyb.ADC(self.ym)
        return(self.xADC.read()/4095*self.length-self.xc-self.xcal)
    
    def y_scan(self):
        '''
        Reads y position
        '''
        self.xm.init(mode = pyb.Pin.ANALOG)
        self.xp.init(mode = pyb.Pin.IN)
        self.ym.init(mode = pyb.Pin.OUT_PP, value = 0)
        self.yp.init(mode = pyb.Pin.OUT_PP, value = 1)
        
        self.pause(5)
        self.yADC = pyb.ADC(self.xm)
        return(self.yADC.read()/4095*self.width-self.yc-self.ycal)
    
    def z_scan(self):
        '''
        Sense if anything is in contact with touch panel
        '''
        self.xm.init(mode = pyb.Pin.OUT_PP, value = 0)
        self.yp.init(mode = pyb.Pin.OUT_PP, value = 1)
        
        self.ym.init(mode = pyb.Pin.ANALOG)
        
        self.pause(5)
        self.xADC = pyb.ADC(self.ym)       
        
        if self.xADC.read() >= 4080:
            return False
        elif self.xADC.read() < 4080:
            return True
        else:
            print('Error in z scan')
    
    def scan_all(self):
        '''
        Reads x, y, and z
        '''
        #Setup for x scan
        self.xm.init(mode = pyb.Pin.OUT_PP, value = 0)
        self.xp.init(mode = pyb.Pin.OUT_PP, value = 1)
        self.ym.init(mode = pyb.Pin.ANALOG)
        self.yp.init(mode = pyb.Pin.IN)        
        
        self.xADC = pyb.ADC(self.ym)
        self.pause(5)
        __x__ = self.xADC.read()/4095*self.length-(self.xc + self.xcal) 
        
        #Setup for z scan
        self.yp.init(mode = pyb.Pin.OUT_PP, value = 1)
        self.pause(5)
        
        if self.xADC.read() >= 4060:
            __z__ = False
        # elif self.xADC.read() < 4000:
        #    __z__ = True
        else:
            __z__ = True
        #Setup for y scan
        self.xm.init(mode = pyb.Pin.ANALOG)
        self.xp.init(mode = pyb.Pin.IN)
        self.ym.init(mode = pyb.Pin.OUT_PP, value = 0)        
        self.yADC = pyb.ADC(self.xm)
        
        self.pause(5)
        __y__ = self.yADC.read()/4095*self.width-(self.yc + self.ycal)
                                      
        return(__x__/1000, __y__/1000, __z__)

    def pause(self,duration):
        '''Pauses code for a certain duration 
        @param duration at which to pause code at
        '''
        now = utime.ticks_us()
        while True:
            if utime.ticks_diff(utime.ticks_us(),now) >= duration:
                return None
    
    def cal(self):
        ''' Calibrate resistive touch sensor
        '''
        pos = self.scan_all()
        self.xcal = pos[0] *1000
        #print("X calibration: " + str(self.xcal))
        self.ycal = pos[1] *1000
        #print("Y calibration: " + str(self.xcal))
        
        
if __name__ == '__main__':    
    # This portion is for testing purposes only 
    #Setting up hardware
    ##@brief UART port
    myuart = pyb.UART(2)
    pyb.repl_uart(None)
    ##@brief ym pin
    PA0 = pyb.Pin.cpu.A0
    ##@brief xm pin
    PA1 = pyb.Pin.cpu.A1
    ##@brief yp pin
    PA6 = pyb.Pin.cpu.A6
    ##@brief xp pin
    PA7 = pyb.Pin.cpu.A7
    ##@brief TouchDriver instance
    TouchD = TouchDriver(PA0,PA1,PA6,PA7, 176, 100, [86.5,51.29])
    ##@brief timestamp for periodically running code
    now = utime.ticks_ms()
    #Prints (x,y) coordinates every 50 ms assuming that something is in contact with the panel
    while True:
        if utime.ticks_diff(utime.ticks_ms(), now)>50:
            now = utime.ticks_ms()
            #print('x:' + str(TouchD.x_scan()))
            #print('y:' + str(TouchD.y_scan()))
            if TouchD.z_scan():
                #print('Touching')
                ##@brief most current readings
                j = TouchD.scan_all()
                print(str(j[0]) + ', ' + str(j[1]))
            else:
                pass
                #print('Not Touching')
            #print(TouchD.scan_all())