'''@file        main.py
@brief          makes use of EncoderDriver.py, MotorDriver.py, and TouchDriver.py to write tasks to create a self balancing platform
@details        Source code can be found at https://bitbucket.org/aartran24/me405/src/master/405%20Term%20Project/main.py
@author         Aaron Tran
@date           6/10/21
'''

from EncoderDriver import EncoderDriver
from TouchDriver import TouchDriver
from MotorDriver import DRV8847

import pyb

from pyb import USB_VCP

from array import array
import math
import cotask
import gc
import utime

# =========================HARDWARE CONFIGURATION==============================
#Motors

##@brief Motor 1 INx pin
IN1 = pyb.Pin.cpu.B4
##@brief Motor 1 INy pin
IN2 = pyb.Pin.cpu.B5
##@brief Motor 2 INx pin
IN3 = pyb.Pin.cpu.B0
##@brief Motor 2 INy pin
IN4 = pyb.Pin.cpu.B1

##@brief nSLEEP pin for both motors    
nSLEEP = pyb.Pin.cpu.A15
##@brief nFAULT pin for both motors
nFAULT = pyb.Pin.cpu.B2
##@brief timer to be used for both motors
tim3 = pyb.Timer(3, freq=20000)

##@brief DRV8847 instance
mot = DRV8847(nSLEEP, nFAULT)
##@brief DRV8847_channel instance corresponding to Motor 1
mot1 = mot.channel(IN1,IN2,tim3)
##@brief DRV8847_channel instance corresponding to Motor 2
mot2 = mot.channel(IN3,IN4,tim3)

#Encoders
##@brief Encoder 1 pin 1
ENC1_1 = pyb.Pin.cpu.B6
##@brief Encoder 1 pin 2
ENC1_2 = pyb.Pin.cpu.B7
##@brief Encoder 2 pin 1
ENC2_1 = pyb.Pin.cpu.C6
##@brief Encoder 2 pin 2
ENC2_2 = pyb.Pin.cpu.C7

##@brief Timer 4 object, to be used for Encoder 1
tim4 = pyb.Timer(4, period=0xFFFF, prescaler=0)
##@brief Timer 8 object, to be used for Encoder 2
tim8 = pyb.Timer(8, period=0xFFFF, prescaler=0)

##@brief EncoderDriver instance corresponding to Encoder 1
enc1 = EncoderDriver(ENC1_1, ENC1_2, tim4)
##@brief EncoderDriver instance corresponding to Encoder 2
enc2 = EncoderDriver(ENC2_1, ENC2_2, tim8)

enc1.set_position(0)
enc2.set_position(0)

##@brief timestamp of last encoder tick
t_last_ENC = utime.ticks_ms()
#Touch Panel

##@brief ym pin
PA0 = pyb.Pin.cpu.A0
##@brief xm pin
PA1 = pyb.Pin.cpu.A1
##@brief yp pin
PA6 = pyb.Pin.cpu.A6
##@brief xp pin
PA7 = pyb.Pin.cpu.A7

##@brief Instance of TouchDriver
TouchD = TouchDriver(PA0,PA1,PA6,PA7, 176, 100, [86.5,51.29])

##@brief Flag for DAQ to switch states
next_state = False
# ===========================GLOBAL VARIABLES==================================
##@brief position of the ball (x,y,z)
x = (0,0,False)
##@brief ball velocity (vx,vy)
x_dot = (0,0)
##@brief angle of table (thetax, thetay)
theta = (0,0)
##@brief angular velocity of table (omegax, omegay)
omega = (0,0)
##@brief angle of encoders (theta_enc1, theta_enc2)
theta_m = (0,0)
##@brief mangular velocity of encoders (omega_enc1, omega_enc2)
omega_m = (0,0)
##time of last position check
t_last_TOUCH = utime.ticks_ms()

##USB VCP object
usb = USB_VCP()

#Hardware/ Controller Parameters
##@brief length of motor arm [mm]
rp = 60
##@brief horizontal distance from table pivot to pushrod attachment point [mm]
lp = 110
##@brief motor armature resistance [ohms]
R = 2.21
##@brief torque constant of motors [N-m/A]
Kt = 0.0138
##@brief Unattentuated motor voltage( before PWM) [V]
Vdc = 12
##@brief Gain conversion factor [PWM%/N-m]
K_conv = 100*R/(4*Kt*Vdc)

#state_vec = [x, theta, x_dot, omega]'
##@brief Fullstate feedback gain for motor 1 
Ky =  [-0.50*2.2, -0.29*3.45, -0.18*2.25, -0.04*1.5]#0.43]
##@brief Fullstate feedback gain for motor 2
Kx =  [ 0.50*2.2, -0.29*3.45,  0.18*2.25, -0.04*1.5]#0.43]

#Input the conversion and adjust for motor differences
Kx = [ 1.05* xgain * K_conv for xgain in Kx]
Ky = [ ygain * K_conv for ygain in Ky]



#This comes out to
#Ky = [-356.9897, -335.7371, -155.5408, -12.67814]
#Kx = [374.8392, -352.524, 163.3178, -13.31205]
##@brief motor PWM duty (PWM_mot1, PWM_mot2)
mot_duty = [0,0]
##@brief Positional gain tuning constant
tune_pos = 1.05
##@brief Speed gain tuning constant
tune_sp = 1.15
# ===========================TASK DEFINITION===================================
def ControllerTask():
    '''@brief Implements closed loop full state feedback controller 
    '''
    global x
    global x_dot
    global theta
    global omega
    global mot_duty 
    global Ky
    global Kx
    global mot1_cal
    global mot2_cal
    global tune_pos
    global tune_sp
    #Calculate motor PWM
    while True:  
        #if object on panel
        if x[2]: 
            mot_duty[0] = -(tune_pos*Ky[0]*x[1]  +  tune_pos*Ky[1]*theta[0]  +  tune_sp*Ky[2]*x_dot[1]  +  tune_sp*Ky[3]*omega[0])
            mot_duty[1] = -(tune_pos*Kx[0]*x[0]  +  tune_pos*Kx[1]*theta[1]  +  tune_sp*Kx[2]*x_dot[0]  +  tune_sp*Kx[3]*omega[1])
            
        else:
            mot_duty[0] = -(  tune_pos*Ky[1]*theta[0]  +  tune_sp*Ky[3]*omega[0])
            mot_duty[1] = -(  tune_pos*Kx[1]*theta[1]  +  tune_sp*Kx[3]*omega[1])
            
        #Adjust for stiction
        __offset__ = 5.5
        if mot_duty[0]>__offset__:
            mot_duty[0] += mot1_cal
        elif mot_duty[0]<-1*__offset__:
            mot_duty[0] -= mot1_cal
        elif mot_duty[0]>=-1*__offset__ and mot_duty[0]<=__offset__:
            mot_duty[0] = mot1_cal/__offset__ * mot_duty[0]
        if mot_duty[1]>__offset__:
            mot_duty[1] += mot2_cal
        elif mot_duty[1]<-1*__offset__:
            mot_duty[1] -= mot2_cal
        elif mot_duty[1]>=-1*__offset__ and mot_duty[1]<=__offset__:
            mot_duty[1] = mot2_cal/__offset__ * mot_duty[1]            
            
        yield(None)
    


def TouchTask():
    '''@brief Reads touch panel to return ball position and velocity
    '''    
    global x
    global x_dot
    global t_last_TOUCH
    while True: 
        ##@brief most recent resistive touch panel readings
        new_pos = TouchD.scan_all()
        #print('current position reading:' + str(new_pos))
        
        #retrieve ball velocity
        ##@brief time since touch panel was last read
        dt = utime.ticks_diff(utime.ticks_ms(), t_last_TOUCH)/1000
        if new_pos[2]:
            x_dot = ((new_pos[0]-x[0])/dt, (new_pos[1]-x[1])/dt)
            #print('current velocity reading:' + str(x_dot))
        else:
            x_dot = (0,0)
        t_last_TOUCH = utime.ticks_ms()
        #retrieve ball position
        x = new_pos
        yield(None)


def MotorTask():
    '''@brief Interfaces with motors to deliver PWM signal
    '''
    global mot_duty
    while True:
        mot1.set_level(mot_duty[0])
        mot2.set_level(mot_duty[1])

        yield(None)


def EncoderTask():
    '''@brief Reads encoder position and translates it into table angle and angular velocity
    '''
    global theta_m
    global theta
    global omega_m
    global omega
    global t_last_ENC
    global rp
    global lp
    while True:
        enc1.update()
        enc2.update()
        ##@brief most recent encoder 1 reading
        new_e1 = enc1.get_position()
        ##@brief most recent encoder 2 reading
        new_e2 = enc2.get_position()
        #retrieve motor positions
        t_now_ENC = utime.ticks_ms()
        ##@brief time since last encoder reading
        dt = utime.ticks_diff(t_now_ENC, t_last_ENC)/1000
        omega_m = ((new_e1-theta_m[0])/dt, (new_e2-theta_m[1])/dt )
        t_last_ENC = t_now_ENC
        #update values
        omega = (-omega_m[0]* rp/lp, -omega_m[1]* rp/lp) 
        theta_m = (new_e1, new_e2)
        theta = (-theta_m[0]* rp/lp, -theta_m[1]* rp/lp) 
        yield(None)


def UI_Task():
    '''@brief Handles all user interactions with program
    '''
    global x
    global x_dot
    global theta
    global omega
    global mot_duty
    global next_state
    global tune_pos
    global tune_sp
    while True:
        #detects if any characters have been sent through the USB
        if usb.any():
            ##@brief characters that have been sent through the USB
            char = usb.recv(data = 1)
            if char == b'c':
                mot.clear_fault()
                print('Clearing fault')
            elif char == b'i':
                mot.enable()
                print('Enabling motors')
            elif char == b'o':
                mot.disable()
                print('Disabling motors')
            elif char == b'z':
                next_state = True
            elif char == b'x':
                print(mot_duty)
            elif char == b'v':
                print('Position (mm): (' +  str(x[0]*1000) +  ', ' + str(x[1]*1000) + ')')
                print('Velocity (mm/s): ' +  str(x_dot[0]) +  ', ' + str(x_dot[1]) + ')')
                print('Theta (deg): (' +  str(theta[0]*180/math.pi) + ', ' + str(theta[1]*180/math.pi) + ')' )
                print('Omega (deg/s): (' +  str(omega[0]*180/math.pi) + ', ' + str(omega[1]*180/math.pi) + ')')
            elif char == b'w':
                tune_pos +=0.05
                print('Position tuning constant:' + str(tune_pos))
            elif char == b's':
                tune_pos -=0.05
                print('Position tuning constant:' + str(tune_pos))       
            elif char == b'd':
                tune_sp +=0.05
                print('Speed tuning constant:' + str(tune_sp))
            elif char == b'a':
                tune_sp -=0.05
                print('Speed tuning constant:' + str(tune_sp))          
            elif char == b'e':
                enc1.set_position(0)
                enc2.set_position(0)  
                print("Encoders have been zeroed")
            elif char == b'q':
                TouchD.cal()
                print('Ball position has been zeroed')
        yield(None)


def DAQ():
    '''@brief Collect and store data
    '''
    global x
    global x_dot
    global next_state
    global x_dat
    global y_dat
    global time
    global __n__
    ##@brief mode of DAQ task, init for clearing/creating vectors, collecting for data collection
    DAQ_mode = 'init'
    while True:
        if DAQ_mode == 'init':
            ##@brief vector of x position data
            #x_dat = 700*[0]
            x_dat = array('f', 1000*[0])
            ##@brief vector of y position data
            #y_dat = 700*[0]
            y_dat = array('f', 1000*[0])
            ##@brief timestamp of each time data is collected
            #time = 700*[0]
            time = array('f', 1000*[0])
            if next_state:
                ##@brief 'zero' time for dataset
                t0 = utime.ticks_ms()
                next_state = False
                DAQ_mode = 'collecting'
                print('Beginning data collection')
                __n__=0
        elif DAQ_mode == 'collecting':
            if x[2]:
                if __n__ <= 699:    
                    x_dat[__n__] = x[0] * 1000
                    y_dat[__n__] = x[1] * 1000
                    time[__n__] = utime.ticks_diff(utime.ticks_ms(),t0)/1000
                    __n__ +=1
                    __flag__ = True
                else: 
                    if __flag__:
                        print('Arrays are full, data collection has ended')
                        __flag__ = False
                    else:
                        pass
            if next_state:
                next_state = False
                DAQ_mode = 'init'
                print('Erasing data')
            
        yield(None) 
        
def Stiction():
    ''' Determines the amount of stiction to the motor to calibrate PWM
    '''
    global mot1_cal
    global mot2_cal
    ##@brief initial PWM for motor 1
    start1 = 10
    ##@brief initial PWM for motor 2
    start2 = 10
    ##@brief timestamp for periodically running code
    now = utime.ticks_ms()
    input('Level platform then hit enter to calibrate motor 1')
    mot1.set_level(0)
    mot2.set_level(0)
    mot.enable()
    enc1.update()
    enc1.set_position(0)
    while abs(enc1.get_position()) <0.005:
        if utime.ticks_diff(utime.ticks_ms(), now)>=5:
            enc1.update()
            print(enc1.get_position())
            now = utime.ticks_ms()        
            mot1.set_level(start1)
            mot1_cal = start1            
            start1+=0.1
    ##@brief motor 1 calibration constant
    mot1_cal = mot1_cal*0.8
    mot1.set_level(0)
    print('Stiction ends for motor 1 at a PWM of ' + str(mot1_cal))
    input('Level platform then hit enter to calibrate motor 2')
    enc2.update()
    enc2.set_position(0)
    while abs(enc2.get_position()) <0.005:
        if utime.ticks_diff(utime.ticks_ms(), now)>=5:
            enc2.update()
            print(enc2.get_position())
            now = utime.ticks_ms()        
            mot2.set_level(start2)
            mot2_cal = start2            
            start2+=0.1    
    ##@brief motor 2 calibration constant
    mot2_cal = mot2_cal*0.8
    mot2.set_level(0)
    mot.disable()
    
    mot1_cal = 20
    mot2_cal = 25
    print('Stiction ends for motor 2 at a PWM of ' + str(mot2_cal))
    print('Hit i at any time to begin balancing')

# ==============================MAIN CODE======================================
if __name__ == "__main__":
    ##@brief touch panel task    
    task_touch = cotask.Task (TouchTask, name = 'TouchTask', priority = 3, 
                         period = 20, profile = True, trace = False)
    ##@brief encoder task
    task_encoder = cotask.Task (EncoderTask, name = 'EncoderTask', priority = 3, 
                         period = 20, profile = True, trace = False)
    ##@brief motor task
    task_motors = cotask.Task (MotorTask, name = 'MotorTask', priority = 1, 
                         period = 40, profile = True, trace = False)
    ##@brief controller task
    task_control = cotask.Task (ControllerTask, name = 'ControllerTask', priority = 2, 
                         period = 40, profile = True, trace = False)
    ##@brief UI task
    task_UI = cotask.Task (UI_Task, name = 'UI_Task', priority = 0 , 
                         period = 40, profile = True, trace = False)
    
    ##@brief DAQ task
    task_DAQ = cotask.Task (DAQ, name = 'DAQ_Task', priority = 0, 
                         period = 20, profile = True, trace = False)
    
    # Add tasks to the task list
    cotask.task_list.append (task_touch)    
    cotask.task_list.append (task_encoder)
    cotask.task_list.append (task_motors)
    cotask.task_list.append (task_UI)
    cotask.task_list.append (task_control)
    cotask.task_list.append (task_DAQ)
    
    # Run the memory garbage collector to ensure memory is as defragmented as
    # possible before the real-time scheduler is started
    gc.collect ()
    
    
    
    try:
        __n__ = 0
        #Motors initially disabled to make less annoying
        mot.disable()
        #Stiction()
        print('Code has been initialized, press i at anytime to begin balancing')
        mot1_cal = 20
        mot2_cal = 22
        while True:
            cotask.task_list.pri_sched ()
    except KeyboardInterrupt:
        mot.disable()
        print('showing stuff')
        print(cotask.task_list.__repr__())
        #Curtail extraneous zeros in data from preallocation
        time = time[0:__n__]
        x_dat = x_dat[0:__n__]
        y_dat = y_dat[0:__n__]
        #Write collected data to file            
        with open("balance_data.csv","w") as a_file:
                a_file.write("Time [sec], x [mm], y [mm]\r")
                for n in range(len(time)):
                    a_file.write("{time}, {x}, {y}\r".format(time = time[n], x = x_dat[n], y = y_dat[n] ))           
                    print("{time}, {x}, {y}\r".format(time = time[n], x = x_dat[n], y = y_dat[n] ))

        
        
